#!/bin/bash

#flagger is a short program coded in bash and needed to define the status of the project as well as the day of use.

usage="\nUsage: $0 [-d directoryOfProject] [-s status] \n
-d\tpath to the directory where to flag status. Mandatory argument.
-s\tstatus of the project folder to use for the flag. Mandatory argument.\n"

while getopts :d:s:h opt   #critical point all the arguments are put and followed by : if they expect an argument so p: says that it expect an argument after so for the help message we want it empty so we need to write h and NOT h: .the first : before all arguments is to avoid getopts to throw any automatic errors!!!!
do
	case $opt in
	d) directoryOfProject=$OPTARG
	;;
	s) status=$OPTARG
	;;
	h) printf "$usage" >&2
           exit 1
        ;;
	'?') printf "$0: invalid option -$OPTARG" >&2
	     printf "$usage" >&2
	     exit 1
	     ;;
	esac
done


pathToScripts=${0%'flagger.sh'}

flags=$(cat $pathToScripts/.flags | awk '{print $1}')

test=$(echo $flags | grep "$status" | wc -l)

if [ $test == 1  ]
then
    date=$(date "+%Y-%m-%d")
    exists=$(cat $directoryOfProject/.flags | awk '{print $1}' | grep "$status" | wc -l)
    if [ $exists == 1  ]
    then
	echo 'Flag as already be inserted previously. Either remove the .flags file or check the flags'
    else
	printf "$status\t$date\n" >> $directoryOfProject/.flags
	echo 'flag inserted'
    fi
else
    echo "flag non inserted as it is not part of $flags"
    echo "If you want to add a new flag, please modify the .flags file of this tool utilities or check the available flags using info_flags.sh"
fi
