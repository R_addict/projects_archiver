#!/bin/bash

#a simpl

usage="\nUsage: $0 [-d directoryOfProject] [-s status] \n
-d\tpath to the directory to initiate. Mandatory argument.\n"

while getopts :d:h opt   #critical point all the arguments are put and followed by : if they expect an argument so p: says that it expect an argument after so for the help message we want it empty so we need to write h and NOT h: .the first : before all arguments is to avoid getopts to throw any automatic errors!!!!
do
	case $opt in
	d) directoryOfProject=$OPTARG
	;;
	h) printf "$usage" >&2
           exit 1
        ;;
	'?') printf "$0: invalid option -$OPTARG" >&2
	     printf "$usage" >&2
	     exit 1
	     ;;
	esac
done


mkdir $directoryOfProject
date=$(date "+%Y-%m-%d")
printf "init\t$date\n" >> $directoryOfProject/.flags
echo 'project initialized'






