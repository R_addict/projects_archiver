#!/bin/bash

#archiver is only program that checks the folders in the path defined and looks for flags to archive what is too hold and return a report file to say how many projcts have been archived.

pathToScripts=${0%'archiver.sh'}

source $pathToScripts/.archiving_settings

for projectsFlags in $folderToWatch/*/.flags
do
    projectFolder=${projectsFlags%'.flags'}
    echo $projectFolder
    lastFlag=$( tail -n 1 $projectsFlags)
    flag=$( echo $lastFlag | awk '{print $1}')
    lastDate=$( echo $lastFlag | awk '{print $2}')
    today=$(date "+%Y-%m-%d")
    echo $flag
    if [[ $flag == $flagForArchive ]]
    then
	daysSinceFinished=$(((`date -jf "%Y-%m-%d" "$today" +%s` - `date -jf "%Y-%m-%d" "$lastDate" +%s`)/86400))
	if  [[ "$daysSinceFinished" > "$durationAfterFlag"  ]]
	then
	    mv $projectFolder $folderToArchive
	    printf "$projectFolder moved to $folderToArchive on the $today\n"  >>$pathToScripts/archiver_log
	fi
	
    elif  [[ $flag == $flagForDirArchive  ]]
    then
	mv $projectFolder $folderToArchive
	printf "$projectFolder moved to $folderToArchive on the $today\n"  >>$pathToScripts/archiver_log
    else
	echo "$projectFolder not moved as not finished"
    fi
    
done

